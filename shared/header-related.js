import React from 'react';
import {View, Image, Text, 
    TouchableOpacity,
    StyleSheet}
    from 'react-native'


export default function ListHeader({ navigation }) {
    
    return (
        <View 
            style={sty.container}>
            <TouchableOpacity 
                style={sty.menuIcon}
                onPress={() => navigation.openDrawer()}
            >
                <Image
                    style={sty.image} 
                    source={require('../assets/menu/hdpi/menu.png')}/>
            </TouchableOpacity>
            <Text style={sty.title}>{'s-Kite Surfing'}</Text>
            <TouchableOpacity 
                style={sty.filterIcon}
                onPress={() => navigation.navigate('Filter')}
            >
                <Image 
                    style={sty.image}
                    source={require('../assets/filter/hdpi/filter.png')}/>
            </TouchableOpacity>
        </View>
    );
}

const sty = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',

        justifyContent: 'center',
        alignItems: 'center', 
        backgroundColor: '#ffb84d',
        flexDirection:'row'
    },
    title: {
        margin: 20,
        fontSize: 30
    },
    menuIcon: {
        position: 'absolute',
        left: 10,

    },
    filterIcon: {
        position: 'absolute',
        right: 10,
        
    },
    image: {
        height: 40,
        width: 40
    }
});

// export default function ListHeader(params) {
//     return (
//       <View style={sty.cont}>
//         <Text>{'My dummy app'}</Text>
//         <TouchableOpacity 
//           style={sty.btn} 
//         //   onPress={() => params.navigation.navigate('Filter')}
//         >
//         <Text>{'Do something'}</Text>
//         </TouchableOpacity>
//       </View>
//     );
//   }
  
//   const sty = {
//     cont: {
//       alignItems: 'center',
//       alignContent: 'center',
//       backgroundColor: 'white',
//       padding: 25
//     },
//     title: {
      
//     },
//     btn: {
//       position: 'absolute',
//       right: 5,
//       top: '25%',
//       fontSize: 16,
//       padding: 5,
//       width: '50',
//       backgroundColor: 'blue',
//       color: 'white',
//     }
//   };