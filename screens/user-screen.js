import React, { Component } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import User from '../model/user';

export default class UserScreen extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            currentUser: this.props.route.params.user
        };
    }

    render() {
        return (
            <View style={sty.outerContainer}>
                <Image
                    style={{width: 100, height: 100, alignSelf: 'center'}}
                    source={{uri: this.state.currentUser.avatar}}    
                />
                <Text style={sty.nameTxt }>{this.state.currentUser.name}</Text>
            </View>
        );
    }
}

const sty = StyleSheet.create({
    outerContainer: {
        flex: 1,
        justifyContent: 'space-evenly',
        alignContent: 'flex-start'
    },
    nameTxt: {
        fontSize: 25,
        fontWeight: 'bold',
        alignSelf: 'center'
    }
});