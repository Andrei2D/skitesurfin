import React from 'react';
import { TextInput } from 'react-native-gesture-handler';
import DataManager from '../model/data-manager';
import User from '../model/user';
import { 
    Text, 
    View, 
    Image,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Keyboard
} from 'react-native';



export default class LoginScreen extends React.Component {
    WAITING = 'w';
    CHECKING = 'c';
    SUCCES = 's';
    FAILED = 'nf';
    

    constructor(props) {
        super(props);
        this.userMng = new DataManager(User);
        this.state = {
            stateValue: this.WAITING,
            email: '',
            password: '',
            blockView: false
        };
        this.passUser = props.passUser;
    }

    hasOverlay() {
        return this.state.blockView;
    }

    toggleOverlay() {
        this.setState({
           blockView: !this.state.blockView
        });
    }

    setEmail(text) {
        this.setState({
            email: text
         });
    }

    setPass(text) {
        this.setState({
            password: text
         });
    }

    wanrMessage() {
        switch(this.state.stateValue) {
            case this.SUCCES:
                return (
                    <Text style={{alignSelf: 'center', color: 'darkgreen'}}>
                        {'Login succesful.'}
                    </Text>
                );
            case this.FAILED:
                return (
                    <Text style={{alignSelf: 'center', color: 'red'}}>
                        {'Login failed.'}
                    </Text>
                );
            default:
                return (<Text></Text>);
        }
    }

    async loginAction() {
        Keyboard.dismiss();
        this.setState({
            stateValue: this.CHECKING
        });
        let users = await this.userMng.getAll();
        let myuser = undefined;
        let mail = this.state.email;

        for(let user of users) {
            if(mail.toLowerCase() == user.email.toLowerCase()) {
                myuser = user;
                break;
            }
        }
        if(myuser == undefined) {
            this.setState({
                stateValue: this.FAILED
            });
        }
        else {
            this.setState({
                stateValue: this.SUCCES
            });
            setTimeout(() => this.passUser(myuser), 300);
        }
    }


    extraView() {
        switch(this.state.stateValue) {
            case this.WAITING:
                return;
            case this.CHECKING: 
            return (
                <View style={sty.overlayView}>
                <ActivityIndicator 
                        style={sty.inidcator}
                        size="large" 
                        color="lightblue" />
                </View>);
            
            default:
                return;
        }
    }

    render() {
        return (
        <View style={sty.outContainer}>
            <Image 
                style={sty.bgImg} 
                source={require('../assets/kite_surf_walp.png')} />
            <Text style={sty.topText}>{"s-Kite Divin'"}</Text>
            {this.wanrMessage()}
            <Text style={sty.label}>{"email:"}</Text>
            <TextInput 
                autoCapitalize={'none'}
                style={sty.inputSt}
                onChangeText={(txt) => this.setEmail(txt)}/>
            <Text style={sty.label}>{"password:"}</Text>
            <TextInput 
                autoCapitalize={'none'}
                style={sty.inputSt} 
                secureTextEntry={true}
                onChangeText={(txt) => this.setPass(txt)}
                />
            <TouchableOpacity 
                style={sty.buttonSt}
                onPress={()=>this.loginAction()}>
                <Text style={sty.buttonTxt}>{"LOG IN"}</Text>
            </TouchableOpacity> 

            {this.extraView()}
        </View>
        );
    }
}

const sty = StyleSheet.create({
    outContainer: {
        margin: 10,
        flex: 1,
        justifyContent: 'center'
        
    },
    topText: {
        fontSize: 40,
        alignSelf: 'center', 
        padding: 10,
        backgroundColor: 'lightblue',
        borderRadius: 25,

        color: 'blue',
        fontWeight: 'bold',
        fontFamily: 'Impact'
    },
    label: {
        marginTop: 10,
        marginHorizontal: 25,
        color: 'lightslategrey'
    }, 
    inputSt: {
        margin: 3,
        borderWidth: 2,
        textAlign: 'center',
        backgroundColor: 'gold'
    },
    buttonSt: {
        alignSelf: 'center',
        backgroundColor: 'lightblue',

        borderWidth: 2,
        borderColor: 'blue',

        marginTop: 20,
        paddingHorizontal: '40%',
        paddingVertical: 20,
    },
    buttonTxt: {
    },
    overlayView: {
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',

        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        zIndex: 2,

        width: '120%',
        height: '120%',
    },
    inidcator: {
        position:'absolute',
        alignSelf: 'center',
    },
    bgImg: {
        zIndex: -1,
        position: 'absolute',
        alignSelf: 'center',
        width: '120%',
        top: -10
    }
});