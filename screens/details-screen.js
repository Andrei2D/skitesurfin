import React from 'react';
import Spot from '../model/spot';
import { View, StyleSheet, Text } from 'react-native';



function Field(desc, descSty, txt, txtSty) {
    return (
        <View>
            <Text style={descSty}>
                {desc}
            </Text>
            <Text style={txtSty}>
                {txt}
            </Text>
        </View>
    );
}

function BigField(props) {
    let descSty = StyleSheet.compose(sty.description, sty.bigDesc);
    let txtSty = StyleSheet.compose(sty.text, sty.bigTxt);
    return Field(props.desc, descSty, props.txt, txtSty);
}

function SmallField(props) {
    return Field(props.desc, sty.description, props.txt, sty.text);
}

function Details(props) {
    let text = new Spot();
    for(let prp in text) {
        text[prp] = props.spot[prp] == '' ? '-' 
            : props.spot[prp];
    }

    return (
    <View style={sty.outContainer}>
        
        <View style={sty.upperContainer}>
            <BigField desc={"Name:"} txt={text.name} />
        </View>
        
        <View style={sty.lowerContainer}>
            <SmallField 
                desc={"Country:"} txt={text.country} 
                />    
        </View>
        
        <View style={sty.lowerContainer}>
            <SmallField 
            desc={"Recommended month:"} txt={text.month}
            />
            <SmallField 
            desc={"Wind probability:"} txt={text.probability + " %"}
            />
        </View>

        <View style={sty.lowerContainer}>
            <SmallField 
            desc={"Latitude:"} txt={text.lat +" N"} 
            />
            <SmallField 
            desc={"Longitude:"} txt={text.long + " W"}
            />
        </View>

    </View>
    );
}

export default class DetailsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spot: this.props.route.params.spot
        };
    }

    render() {
        return (<Details spot={this.state.spot} />);
    }

}

// ---------------CSS Styles-----------------

const sty = StyleSheet.create({
    outContainer: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignContent: 'space-around',
        paddingTop: 10
    },
    upperContainer: {
        display: 'flex',
        flexGrow: 1,
        alignItems: 'center',
        margin: 10,
        padding: 30
    },
    lowerContainer: {
        display: 'flex',
        flexGrow: 4,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    description: {
        color: 'grey',
        alignSelf: 'center',
        fontSize: 15
    },
    bigDesc: {
        fontSize: 20,
    },
    text: {
        alignSelf: 'center',
        fontSize: 25,
        fontWeight: 'bold'
    },
    bigTxt: {
        fontSize: 30
    }

});
