import React from 'react';
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import { Component } from "react";


export default class FilterScreen extends React.Component{
    static filterState = () => ({
        country: '',
        month: '',
        wind: ''
    });

    constructor(props) {
        super(props);
        this.state = props.route.params.state;
        this.nav = props.navigation;
    }

    render() {
        return (
        <View style={styles.fltBackground}>
            <View style={styles.fltBody}>
                <Text style={styles.fltTitle}>{"Filter"}</Text>
                <View style={styles.fltContainers}>
                    <Text>{"Country: "}</Text>
                    <TextInput 
                        defaultValue={this.state.country}
                        onChangeText={function(txt){
                            this.setState({
                                country: txt
                            });
                        }.bind(this)}
                        style={styles.fltInputs}
                        />
                </View>
                <View style={styles.fltContainers}>
                    <Text>{"Preffered month: "}</Text>
                    <TextInput 
                        defaultValue={this.state.month}
                        onChangeText={function(txt){
                            this.setState({
                                month: txt
                            });
                        }.bind(this)}
                        style={styles.fltInputs}
                        />
                </View>
                <View style={styles.fltContainers}>
                    <Text>{"Max wind probability: "}</Text>
                    <TextInput 
                        defaultValue={this.state.wind.toString()}
                        onChangeText={function(txt){
                            this.setState({
                                wind: txt
                            });
                        }.bind(this)}
                        style={styles.fltInputs}
                        keyboardType={'numeric'}
                    />
                </View>
                <View style={styles.fltBtnsView}>
                    <Button 
                        onPress={(() => 
                            this.nav.pop()).bind(this)}
                        title={'Cancel'}
                    />
                    <Button
                        onPress={(() => this.nav.navigate('List',
                        {filter: this.state})).bind(this)}
                        title={'   Apply   '}
                    />
                </View>
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    flatList: {
        marginTop: 5
    },
    fltBackground: {
        position: 'absolute',
        alignSelf: 'center',
        backgroundColor: 'rgba(8,8,8,0.5)',
        zIndex: 1,

        width: '100%',
        height: '100%',

        flex:1,
        justifyContent: 'center'

    },
    fltBody: {
        padding: 20,
        width: '60%',

        backgroundColor: 'white',
        position: 'absolute',
        alignSelf: 'center',
        zIndex:2,
        
        flex: 1,
        justifyContent: 'space-around'
    },
    fltTitle: {
        fontSize: 30,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    fltInputs: { 
        borderWidth: 2,
        height: 40,
        textAlign: 'center',
    },
    fltContainers: {
        margin: 2,
    },
    fltBtnsView: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        margin: 10
    },
});