import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';

import DetailsScreen from './details-screen';
import ListScreen from './list-screen';
import LoginScreen from './login-screen';
import UserScreen from './user-screen';
import { StyleSheet } from 'react-native';
import ListHeader from '../shared/header-related'
import User from '../model/user';
import FilterScreen from './filter-screen';

const lStack = createStackNavigator();
const lDrawer = createDrawerNavigator();




function ListStack({ route, navigation }) {
    const fiterState = FilterScreen.filterState();
    return (
        <lStack.Navigator initialRouteName="List">
        <lStack.Screen name="List" component={ListScreen} 
            options={{
                title: 's-Kite Surfing',
                header: ListHeader
            }}/>
        <lStack.Screen name="Details" component={DetailsScreen} 
            options={{
                headerStyle: {
                    backgroundColor: '#ffb84d'
                }
            }}
        />
        <lStack.Screen name='Filter' component={FilterScreen}
            initialParams={{
                state: fiterState
            }}
            options={{
                title:'',
                headerStyle: {
                    backgroundColor: '#ffb84d'
                }
            }}
        />
        </lStack.Navigator>
    );
}

export default class MainStack extends React.Component {
    NOT_LOGGED = 'n';
    LOGGED = 'l';

    constructor(props) {
        super(props);
        this.state = {
            stateValue: this.NOT_LOGGED,
            currentUser: undefined
        };
    }

    logOut() {
        this.setState({
            stateValue: this.NOT_LOGGED,
            currentUser: undefined
        });
    }

    setUser(user) {
        this.setState({
            stateValue: this.LOGGED,
            currentUser: user
        });
        
    }

    moreDrawerButtons(props) {
        return (
            <DrawerContentScrollView {...props}>
              <DrawerItemList {...props} />
              <DrawerItem label="Log out" onPress={() => this.logOut()} />
            </DrawerContentScrollView>
          );
    }

    mainDrawer() {
        return (
        <NavigationContainer>
        <lDrawer.Navigator 
                initialRouteName="List" 
                drawerContent={props => this.moreDrawerButtons(props)}>

            <lDrawer.Screen 
                name="User" 
                component={UserScreen}
                initialParams={{user: this.state.currentUser}}
                options={{
                    drawerLabel: "User details"
                }}
                />
            <lDrawer.Screen name="List" component={ListStack}/>
        </lDrawer.Navigator>
        </NavigationContainer>
        );
    }

    currentScreen() {
        switch(this.state.stateValue) {
            case (this.NOT_LOGGED):
                return (<LoginScreen 
                            passUser={(u) => this.setUser(u)}
                        />);
            case (this.LOGGED): 
                return this.mainDrawer();
            default:
                return (<View><Text>{"Error in MainStack"}</Text></View>);
        }
    }


    render() {
        return this.currentScreen();
    }
}


const sty = StyleSheet.create({
    filterImg: {
        margin: 15,
    }
});