import React from 'react';
import Spot from '../model/spot';
import Favourites from '../model/favourites';
import DataManager from '../model/data-manager';
import {FlatList, View, Text, Image, TouchableNativeFeedback} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {ToastAndroid} from 'react-native';

function SpotItem(props) {
    var navFunction = 
        () => (props.nav.navigate('Details', {spot: props.spot}));
    var adder = () => props.adder(props.index);
    var rmer = () => props.rmer(props.index);

    let starButton = props.fav == undefined ?
    (
        <TouchableOpacity
            style={styles.imageContainer}
            onPress={adder}

        >
            <Image 
                source={require('../assets/star-off/hdpi/star-off.png')} 
            />
        </TouchableOpacity>
    ) :
    (<TouchableOpacity
            style={styles.imageContainer}
            onPress={rmer}
    >
            <Image 
                style={styles.starImg}
                source={require('../assets/star-on/mdpi/star-on.png')} 
            />
    </TouchableOpacity>);
    
    let countryText = props.spot.country == '' ? '-' :
        props.spot.country;

    return (
    <View
        key={props.spot.id} 
        style={styles.listItemContainer}
    >
        <TouchableNativeFeedback
            onPress={() => navFunction()}
            style={styles.textTouchable}
        >
            <View
                style={styles.textContainer}
            >
                <Text style={styles.bigListItemTxt}>
                    {props.spot.name}
                </Text>
                <Text>
                    {countryText}
                </Text>
            </View>
        </TouchableNativeFeedback>

        {starButton}
    </View>
    );
}

export default class ListScreen extends React.Component{
    
    constructor(props) {
        super(props);
        this.spotMng = new DataManager(Spot);
        this.favMng = new DataManager(Favourites);
        this.state = {
            items: []
        };
    }

    async addToFav(index) {
        let modifItems = this.state.items;
        let newFav = new Favourites();
        newFav.spot = modifItems[index].spot.id;
        newFav.createdAt = (new Date()).toISOString();
        
        let favList = await this.favMng.post(newFav);
        for(let fav of favList) {
            if(newFav.spot == fav.spot) {
                newFav = fav;
                break;
            }
        }

        modifItems[index].fav = newFav;

        this.setState({
            items: modifItems
        });
    }

    async rmFromFav(index) {
        let modifItems = this.state.items;
        let toRm = modifItems[index].fav;

        try{
            let isOk = await this.favMng.delete(toRm);
            if(isOk) {
                modifItems[index].fav = undefined;
                this.setState({
                    items: modifItems
                });
            }
        }
        catch(error) {
            ToastAndroid.show(error.message, ToastAndroid.LONG);
        }
    }

    async update() {
        var favourites = await this.favMng.getAll();
        
        await this.spotMng.getAll()
        .then(data => data.map(elm => {
            for(let favItem of favourites) {
                if(favItem.spot == elm.id) {
                    return ({
                        spot: elm,
                        fav: favItem
                    });
                }
            }
            return ({
                spot: elm,
                fav: undefined
            });
        }))
        .then((data) =>
            this.setState({items: data})
        );
    }

    loadingSpots() {
        this.update();
        return (<Text style={styles.loatTxt}>
            {"Loading spots..."}
            </Text>);
    }

    spotFlatList() {
        return (
        <FlatList 
        style={styles.flatList}
        data={this.state.items} 
        renderItem={(s) => 
            <SpotItem 
            spot={s.item.spot}
            fav={s.item.fav}
            index={s.index}
            adder={(ind) => this.addToFav(ind)}
            rmer={(ind) => this.rmFromFav(ind)}
            nav={this.props.navigation}
            />}
        keyExtractor={(item, index) => 
            item.spot.id.toString() + '-' + index.toString()}
        />);
    }

    render() {
        const filling = this.state.items.length == 0 ?
            this.loadingSpots() :
            this.spotFlatList();
        return filling;
    }
}

// ---------------CSS Styles-----------------

const styles = {
    listItemContainer: {
        flex: 1,
        margin: 5,
        
        borderBottomRadius: 2,
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d7da',

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
        backgroundColor: 'white'
        
    },
    textTouchable: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    textContainer: {
        width: '80%',
        padding: 10,
    },
    imageContainer: {
        alignSelf: 'center',
        padding: 15,
    },
    bigListItemTxt: {
        fontSize: 20
    },
    loatTxt: {
        fontSize: 15,
        color: "grey",
        alignSelf: "center", 
        marginTop: 25
    },
    starImg: {
    }
    
};
