export default class Spot {
    static endpoint = "spot";

    constructor() {
        this.id = undefined;
        this.createdAt = undefined;
        this.name = undefined;
        this.country = undefined;
        this.lat = undefined;
        this.long = undefined;
        this.probability = undefined;
        this.month = undefined;
    }
}