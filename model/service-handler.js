export default class ServiceHandler {
    baseURL = 'https://5ddbb358041ac10014de140b.mockapi.io/';
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    async handResp(resp) {
        if(resp.ok) {
            return await fetch(resp.url)
            .then(data => data.json());
        }
        else {
            throw (new Error(resp.statusText));
        }
            
    }

    async get(id) {
        let out;
        let url = this.baseURL + this.endpoint;
        let getUrl = id == undefined ? url 
            : url + "/" + id;

        let rez = await fetch(getUrl)
        .then((resp) => resp.json())
        .then((data) => {out = data});

        return out;
    }

    async post(object) {
        let postURl = this.baseURL + this.endpoint;

        let options = {
            method: 'POST', 
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(object),
        };

        let resp =  await fetch(postURl, options);
        return await this.handResp(resp);
    }

    async put(object) {
        if (object == undefined) {
            let errMsg = "[ServiceHandler.PUT()] " + 
                "Object passed is undefined.";
            throw (new Error(errMsg));
        }
        else if(!object.hasOwnProperty("id")) {
            let errMsg = "[ServiceHandler.PUT()] " + 
                "Object passed does not have \"id\" property.";
            throw (new Error(errMsg));
        }
            

        let putUrl = this.baseURL + this.endpoint + "/" + object.id;
        let options  = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(object),
        };

        
        let resp =  await fetch(putUrl, options);
        return await this.handResp(resp)
    }

    async delete(id) {
        if(id == undefined) {
            let errMsg = "[ServiceHandler.DELETE()] " + 
                "Argument \"id\" is undefined.";
            throw (new Error(errMsg));
        }
            
        let delUrl = this.baseURL + this.endpoint + "/" + id;

        return await fetch(delUrl, {method: "DELETE",})
    }

}

