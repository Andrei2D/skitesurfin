import {ToastAndroid} from 'react-native';
import ServiceHandler from './service-handler'

export default class DataManager {
    constructor(modelClass) {
        this.modelClass = modelClass;
        this.handler = new ServiceHandler(modelClass.endpoint);
    }

    builder(obj) {
        var newObj = new this.modelClass();

        for(let prop in newObj){
            newObj[prop] = obj[prop];
        }
        return newObj;
    }

    async getAll() {
        let svData = await this.handler.get();
      	
        return svData.map((obj) => this.builder(obj));
    }

    async get(id) {
        return await this.handler.get(id)
        .then(data => this.builder(data));
    }

    async post(obj) {
        try {
            let svData = await this.handler.post(obj);
            return svData.map((obj) => this.builder(obj));
        }
        catch(error) {
            ToastAndroid.show(error.message, ToastAndroid.LONG);
            return undefined;
        }
    }
    
    async put(obj) {
        try {
            let svData = await this.handler.put(obj);

            return svData.map((obj) => this.builder(obj));
        }
        catch(error) {
            ToastAndroid.show(error.message, ToastAndroid.LONG);
            return undefined;
        }
    }

    async delete(toDelete) {
        var id;
        if(typeof(toDelete) == typeof(0)) {
            id = toDelete;
        }
        else if(typeof(toDelete) == typeof(new this.modelClass())
            && toDelete.id != undefined) {
            id = toDelete.id;
        }
        else {
            throw (new Error("[DataManager.delete()] " +
                "Argument (" + toDelete + ") is neither a number nor a " + 
                this.modelClass.name + " object."));
        }
        let resp = await this.handler.delete(id);
        return resp.ok;
    }
}