export default class Favourites {
    static endpoint = "favourites";

    constructor() {
        this.id = undefined;
        this.createdAt = undefined;
        this.spot = undefined;
    }
}