export default class User {
    static endpoint = "user";

    constructor() {
        this.id = undefined;
        this.createdAt = undefined;
        this.name = undefined;
        this.avatar = undefined;
        this.email = undefined;
    }
}