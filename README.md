# s-Kite Surfin'

Mobile application coded in React Native for a kite surfing platform, giving the user the popular places for practicing and details about that place.

#### Tasks completions:

|      | Tasks                             |
| ---- | :-------------------------------- |
| ✔️    | LIST screen with all the spots    |
| ✔️    | DETAILS screen of each spot       |
| ✔️    | data fetched from API             |
| ✔️    | filter button in nav bar          |
| ✔️    | FILTER screen                     |
| ✖️    | data filtering                    |
| ✔️    | mark SPOT as favorite             |
| ✔️    | unmark SPOT from favorites        |
| ✔️    | LOGIN screen for existing emails  |
| ✖️    | offline support                   |
| ✖️    | notch and smaller screens support |

#### Arhitecture

There are 3 main classes that are being used throughout the app: User, Favorites and Spot. All these structures are present in the API. There is an interface that handles REST services called Service Handler whose role is to interact with the mock API. When other classes want to use those 3 types of data, they will instantiate a Data Manager, whose role is to bridge between the models data and the service interface.

![](_extras_/uml/s-Kite_Surfing__data_handling.png)

There are five main screens in this app: LOGIN, LIST, USER, DETAILS, FILTER. The main app represents a stateful class who can either show the login page or the main content of the app, depending if the user has been successfully log in or not. The main flow is provided by a drawer navigator that includes a stack navigator and also gives the options of accessing to the user screen and logging out. The stack navigator includes the list (which is also the main screen), the details screen and the filter screen (accessible by a button in the header).

![](_extras_/uml/s-Kite_Surfing__UI.png)



#### Video demo:



| Login screen                       | List screen and favorites              |
| ---------------------------------- | -------------------------------------- |
| ![](_extras_/demos/v1.0/login.gif) | ![](_extras_/demos/v1.0/list_favs.gif) |



| Drawer and user screen                   | Details and filter screens                  |
| ---------------------------------------- | ------------------------------------------- |
| ![](_extras_/demos/v1.0/drawer_user.gif) | ![](_extras_/demos/v1.0/filter_details.gif) |